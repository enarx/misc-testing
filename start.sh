#!/usr/bin/env bash
set -e
if [ -d "${REGISTRY_PATH}" ]; then
    /bin/cargo-http-registry "${REGISTRY_PATH}" >/dev/null 2>&1 &
    REGISTRY_PID="$!"
    echo "Registry is running in background on PID: ${REGISTRY_PID}"
fi

command_file="$(mktemp)"
echo "Context: '$1'"
echo "Writing commands to: ${command_file}"
echo -e "#!/usr/bin/env bash\nset -e\n" >> "${command_file}"

echo -e "$(/bin/doctest "${HOME}/Install.md" /etc/os-release "$1")" >> "${command_file}"
chmod a+x "${command_file}"

echo -e "Planning to execute the following commands:\n\n"
echo -e "\n###########################################"
cat "${command_file}"
echo -e "###########################################\n\n\n\n"
sleep 5

time "${command_file}"
echo -e "\n\n\n Done!\n\n\n"
sleep 5
