FROM ghcr.io/platten/rust-musl-builder:latest as builder-base
ENV PATH="/root/.cargo/bin:${PATH}"
RUN git clone https://github.com/platten/doctest.git && \
    cd doctest && \
    cargo build --release --target x86_64-unknown-linux-musl --bin doctest && \
    cp target/x86_64-unknown-linux-musl/release/doctest /bin/doctest
RUN git clone https://github.com/d-e-s-o/cargo-http-registry && \
    cd cargo-http-registry && \
    cargo build --release --target x86_64-unknown-linux-musl --bin cargo-http-registry && \
    cp target/x86_64-unknown-linux-musl/release/cargo-http-registry /bin/cargo-http-registry

FROM debian:testing-slim as debian-base
ARG KVM=108
ENV USERNAME="user"
ENV HOME="/home/user"
ENV USERSHELL="/bin/bash"
ENV TZ=UTC
ENV PATH="${PATH}:/${USERNAME}/.cargo/bin"
ENV BACKEND="nil"
ENV DEBIAN_FRONTEND="noninteractive"
ENV CARGO_NET_GIT_FETCH_WITH_CLI="true"
COPY --from=builder-base /bin/doctest /bin/doctest
COPY --from=builder-base /bin/cargo-http-registry /bin/cargo-http-registry
COPY start.sh /bin
RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get install -y --no-install-recommends tzdata ca-certificates sudo git && \
    git config --system user.name "user" && \
    git config --system user.email "user@example.com" && \
    apt-get clean && \
    groupadd -g ${KVM} kvm && \
    useradd -m -d ${HOME} -s ${USERSHELL} -G kvm ${USERNAME} && \
    echo "export BACKEND=${BACKEND}" >> ${HOME}/.bashrc && \
    echo "${USERNAME} ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
USER ${USERNAME}
WORKDIR ${HOME}
CMD [ "bash", "-l", "-c", "/bin/start.sh ${CONTEXT}" ]

FROM ubuntu:latest as ubuntu-base
ARG KVM=108
ENV USERNAME="user"
ENV HOME="/home/user"
ENV USERSHELL="/bin/bash"
ENV TZ=UTC
ENV PATH="${PATH}:/${USERNAME}/.cargo/bin"
ENV BACKEND="nil"
ENV DEBIAN_FRONTEND="noninteractive"
ENV CARGO_NET_GIT_FETCH_WITH_CLI="true"
COPY --from=builder-base /bin/doctest /bin/doctest
COPY --from=builder-base /bin/cargo-http-registry /bin/cargo-http-registry
COPY start.sh /bin
RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get install -y --no-install-recommends tzdata ca-certificates sudo git && \
    git config --system user.name "user" && \
    git config --system user.email "user@example.com" && \
    apt-get clean && \
    groupadd -g ${KVM} kvm && \
    useradd -m -d ${HOME} -s ${USERSHELL} -G kvm ${USERNAME} && \
    echo "export BACKEND=${BACKEND}" >> ${HOME}/.bashrc && \
    echo "${USERNAME} ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
USER ${USERNAME}
WORKDIR ${HOME}
CMD [ "bash", "-l", "-c", "/bin/start.sh ${CONTEXT}" ]


FROM fedora:latest as fedora-base
ARG KVM=108
ENV USERNAME="user"
ENV HOME="/home/user"
ENV USERSHELL="/bin/bash"
ENV TZ=UTC
ENV PATH="${PATH}:/${USERNAME}/.cargo/bin"
ENV BACKEND="nil"
ENV CARGO_NET_GIT_FETCH_WITH_CLI="true"
COPY --from=builder-base /bin/doctest /bin/doctest
COPY --from=builder-base /bin/cargo-http-registry /bin/cargo-http-registry
COPY start.sh /bin
RUN echo "fastestmirror=1" >> /etc/dnf/dnf.conf && \
    dnf upgrade -y && \
    dnf install -y sudo ca-certificates git && \
    git config --system user.name "user" && \
    git config --system user.email "user@example.com" && \
    dnf clean packages && \
    groupadd -g ${KVM} kvm && \
    useradd -m -d ${HOME} -s ${USERSHELL} -G kvm ${USERNAME} && \
    echo "export BACKEND=${BACKEND}" >> ${HOME}/.bashrc && \
    echo "${USERNAME} ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
USER ${USERNAME}
WORKDIR ${HOME}
CMD [ "bash", "-l", "-c", "/bin/start.sh ${CONTEXT}" ]

FROM centos:centos7 as centos7-base
ARG KVM=108
ENV USERNAME="user"
ENV HOME="/home/user"
ENV USERSHELL="/bin/bash"
ENV TZ=UTC
ENV PATH="${PATH}:/${USERNAME}/.cargo/bin"
ENV BACKEND="nil"
ENV CARGO_NET_GIT_FETCH_WITH_CLI="true"
COPY --from=builder-base /bin/doctest /bin/doctest
COPY --from=builder-base /bin/cargo-http-registry /bin/cargo-http-registry
COPY start.sh /bin
RUN sed -i -e '/^$/d' /etc/os-release && \
    yum update -y && \
    yum upgrade -y && \
    yum install -y sudo ca-certificates git && \
    git config --system user.name "user" && \
    git config --system user.email "user@example.com" && \
    yum clean packages && \
    groupadd -g ${KVM} kvm && \
    useradd -m -d ${HOME} -s ${USERSHELL} -G kvm ${USERNAME} && \
    echo "export BACKEND=${BACKEND}" >> ${HOME}/.bashrc && \
    echo "${USERNAME} ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
USER ${USERNAME}
WORKDIR ${HOME}
CMD [ "bash", "-l", "-c", "/bin/start.sh ${CONTEXT}" ]

FROM centos:centos8 as centos8-base
ARG KVM=108
ENV USERNAME="user"
ENV HOME="/home/user"
ENV USERSHELL="/bin/bash"
ENV TZ=UTC
ENV PATH="${PATH}:/${USERNAME}/.cargo/bin"
ENV BACKEND="nil"
ENV CARGO_NET_GIT_FETCH_WITH_CLI="true"
COPY --from=builder-base /bin/doctest /bin/doctest
COPY --from=builder-base /bin/cargo-http-registry /bin/cargo-http-registry
COPY start.sh /bin/start.sh
RUN sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-Linux-* && \
    sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-Linux-* && \
    echo "fastestmirror=1" >> /etc/dnf/dnf.conf && \
    yum update -y && \
    yum upgrade -y && \
    yum install -y sudo ca-certificates git && \
    git config --system user.name "user" && \
    git config --system user.email "user@example.com" && \
    yum clean packages && \
    useradd -m -d ${HOME} -s ${USERSHELL} -G kvm ${USERNAME} && \
    echo "export BACKEND=${BACKEND}" >> ${HOME}/.bashrc && \
    echo "${USERNAME} ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
USER ${USERNAME}
WORKDIR ${HOME}
CMD [ "bash", "-l", "-c", "/bin/start.sh ${CONTEXT}" ]
