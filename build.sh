#!/usr/bin/env bash

readonly CI_REGISTRY="registry.gitlab.com"
readonly CI_PROJECT_PATH="enarx/misc-testing"

export DOCKER_BUILDKIT=1
docker build --squash --compress --target builder-base -t "$CI_REGISTRY/$CI_PROJECT_PATH/builder-base:latest" .
docker build --squash --compress --target ubuntu-base -t "$CI_REGISTRY/$CI_PROJECT_PATH/ubuntu-base:latest" .
docker build --squash --compress --target debian-base -t "$CI_REGISTRY/$CI_PROJECT_PATH/debian-base:latest" .
docker build --squash --compress --target fedora-base -t "$CI_REGISTRY/$CI_PROJECT_PATH/fedora-base:latest" .
docker build --squash --compress --target centos7-base -t "$CI_REGISTRY/$CI_PROJECT_PATH/centos7-base:latest" .
docker build --squash --compress --target centos8-base -t "$CI_REGISTRY/$CI_PROJECT_PATH/centos8-base:latest" .
